const reverseArray = array => {
    const retArray = []
    for (let i = array.length - 1; i >= 0; i--) {
        retArray.push(array[i])
    }
    return retArray
}

const isPalindrome = str => {
    if (typeof str != 'string' || !str) {
        return false
    }
    let lwrStr = str.replace(/\s/g, '').toLowerCase()
    let backCount = lwrStr.length
    for (let i = 0; i < lwrStr.length; i++) {
        backCount--
        if (lwrStr[i] != lwrStr[backCount]) {
            return false
        }
    }
    return true
}
